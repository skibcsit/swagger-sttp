name := "swagger-sttp"

version := "0.1"

scalaVersion := "2.13.1"

val circeVersion = "0.12.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
).map(_ % circeVersion)

libraryDependencies += "io.circe" %% "circe-yaml" % "0.12.0"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-core" % "0.14.3"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-sttp-client" % "0.14.3"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % "0.14.3"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-openapi-circe-yaml" % "0.14.3"
libraryDependencies += "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % "0.14.4"
