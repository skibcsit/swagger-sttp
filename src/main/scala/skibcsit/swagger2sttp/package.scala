package skibcsit

package object swagger2sttp {
  type Paths = Map[String, PathItem]
  type SecurityRequirement = List[Map[String, List[String]]]
  type Responses = Map[String, Either[Response, Reference]]
  type Callback = Map[String, PathItem]
}
