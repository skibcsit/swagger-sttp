package skibcsit.swagger2sttp

import io.circe._
import io.circe.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.{Endpoint, EndpointInfo, EndpointInput, EndpointOutput, endpoint, path, query, stringBody}
import sttp.model.Method

import scala.io.Source
import scala.io.BufferedSource
import scala.util.{Failure, Success, Try}

case object Parser {

  implicit def decodeEither[A, B](implicit decoderA: Decoder[A], decoderB: Decoder[B]): Decoder[Either[A, B]] = {
    c: HCursor =>
      c.as[A] match {
        case Right(a) => Right(Left(a))
        case _ => c.as[B] match {
          case Right(b) => Right(Right(b))
          case _ => Left(DecodingFailure("Either decoding collapsed", c.history))
        }
      }
  }

  def eitherLeft[A, B](collection: Iterable[Either[A, B]]): Iterable[A] = {
    for (item <- collection; if item.isLeft)
      yield item match {
        case Left(value) => value
      }
  }

  def eitherRight[A, B](collection: Iterable[Either[A, B]]): Iterable[B] = {
    for (item <- collection; if item.isRight)
      yield item match {
        case Right(value) => value
      }
  }

  def parseOpenAPI(parse: Function[String, Either[ParsingFailure, Json]])(string: String): Try[OpenAPI] =
    parse(string) match {
      case Left(exception) => Failure(exception)
      case Right(value) => value.as[OpenAPI3] match {
        case Left(exception) => Failure(exception)
        case Right(value) => Success(value)
      }
    }

  val yamlToOpenAPI: String => Try[OpenAPI] = parseOpenAPI(io.circe.yaml.parser.parse)

  val jsonToOpenAPI: String => Try[OpenAPI] = parseOpenAPI(io.circe.parser.parse)

  def toOpenAPI(string: String): Try[OpenAPI] =
    yamlToOpenAPI(string) match {
      case Success(value) => Success(value)
      case Failure(_) => jsonToOpenAPI(string) match {
        case Failure(exception) => Failure(ParsingFailure("File must be either yaml or json", exception))
        case Success(value) => Success(value)
      }
    }

  def parse(bufferedSource: BufferedSource): Try[Iterable[Endpoint[_, _, _, _]]] =
    toOpenAPI(bufferedSource.mkString) match {
      case Failure(exception) => Failure(exception)
      case Success(OpenAPI3(_, components, _, _, _, paths, _, _)) => Success(definePaths(paths, components))
    }

  def parse(fileName: String): Try[Iterable[Endpoint[_, _, _, _]]] =
    Try(Source.fromFile(fileName)) match {
      case Success(inputStream) => parse(inputStream)
      case Failure(exception) => Failure(exception)
    }

  def zipMethods(pi: PathItem): Map[String, Option[Operation]] = Map("get" -> pi.get, "post" -> pi.post,
    "put" -> pi.put, "delete" -> pi.delete, "head" -> pi.head, "patch" -> pi.patch, "trace" -> pi.trace)

  def definePaths(paths: Paths, components: Option[Components]): Iterable[Endpoint[_, _, _, _]] =
    for {
      (path, pathItem) <- paths
      (methodName, method) <- zipMethods(pathItem)
      if method.isDefined
    } yield defineOperation(methodName, path, method.get, components).method(Method.unsafeApply(methodName))


  def defineOperation(method: String, path: String, op: Operation, c: Option[Components]): Endpoint[_, _, _, _] =
    defineOutput(
      definePathInput(
        defineQueryInput(
          defineInfo(endpoint, method, path, op),
          op.parameters.getOrElse(List.empty).map(dereferenceRight(_, c)).filter(_.isDefined).map(_.get),
          c),
        path.split('/'),
        op.parameters.getOrElse(List.empty).map(dereferenceRight(_, c)).filter(_.isDefined).map(_.get),
        c),
      op.responses.map { case (k, v) => (k, dereferenceRight(v, c)) }.filter { case (_, v) => v.isDefined },
      c)


  implicit def dereferenceSchema(r: Reference, c: Option[Components]): Option[Schema] =
    c.getOrElse(Components()).schemas.getOrElse(Map.empty).get(r.$ref.split('/').last) match {
      case Some(Left(value)) => Some(value)
      case _ => None
    }

  implicit def dereferenceResponse(r: Reference, c: Option[Components]): Option[Response] =
    c.getOrElse(Components()).responses.getOrElse(Map.empty).get(r.$ref.split('/').last) match {
      case Some(Left(value)) => Some(value)
      case _ => None
    }

  implicit def dereferenceParam(r: Reference, c: Option[Components]): Option[Parameter] =
    c.getOrElse(Components()).parameters.getOrElse(Map.empty).get(r.$ref.split('/').last) match {
      case Some(Left(value)) => Some(value)
      case _ => None
    }

  def dereferenceRight[A, B](e: Either[A, B], c: Option[Components])(implicit f: (B, Option[Components]) => Option[A]): Option[A] =
    e match {
      case Left(value) => Some(value)
      case Right(value) => f(value, c)
    }

  def defineInfo(ep: Endpoint[_, _, _, _], method: String, path: String, operation: Operation): Endpoint[_, _, _, _] =
    ep.info(EndpointInfo(
      Some(operation.operationId.getOrElse(path + method)),
      operation.summary,
      operation.description,
      operation.tags.getOrElse(List.empty).toVector,
      operation.deprecated.getOrElse(false)))

  @scala.annotation.tailrec
  def definePathInput(ep: Endpoint[_, _, _, _], parts: Iterable[String], params: List[Parameter], c: Option[Components]): Endpoint[_, _, _, _] = {
    if (parts.isEmpty) ep
    else if (parts.head.equals(""))
      definePathInput(ep, parts.tail, params, c)
    else if (!parts.head.contains("{"))
      definePathInput(ep.in(parts.head), parts.tail, params, c)
    else if (!params.exists(_.name.equals(parts.head.substring(1, parts.head.length - 1))))
      definePathInput(ep.in(parts.head.substring(1, parts.head.length - 1)), parts.tail, params, c)
    else
      definePathInput(ep.in(capturePath(params.find(_.name.equals(parts.head.substring(1, parts.head.length - 1))).get, parts.head.substring(1, parts.head.length - 1), c)), parts.tail, params, c)
  }

  def capturePath(p: Parameter, name: String, c: Option[Components]): EndpointInput[_] = {
    dereferenceRight(p.schema.get, c) match {
      case Some(Schema(Some("integer"), Some("int32"), None, None, None)) => path[Int](name)
      case Some(Schema(Some("integer"), Some("int64"), None, None, None)) => path[Long](name)
      case Some(Schema(Some("number"), Some("float"), _, None, None)) => path[Float](name)
      case Some(Schema(Some("number"), Some("double"), _, None, None)) => path[Double](name)
      case Some(Schema(Some("boolean"), None, _, None, None)) => path[Boolean](name)
      case _ => path[String](name)
    }
  }.description(p.description.getOrElse(""))

  @scala.annotation.tailrec
  def defineQueryInput(ep: Endpoint[_, _, _, _], ops: Iterable[Parameter], c: Option[Components]): Endpoint[_, _, _, _] =
    if (ops.isEmpty) ep
    else if (ops.head.in.equals("query")) defineQueryInput(ep.in(captureQuery(ops.head, ops.head.name, c)), ops.tail, c)
    else defineQueryInput(ep, ops.tail, c)

  def captureQuery(p: Parameter, name: String, c: Option[Components]): EndpointInput[_] = {
    dereferenceRight(p.schema.get, c) match {
      case Some(Schema(Some("integer"), Some("int32"), _, None, None)) => query[Int](name)
      case Some(Schema(Some("integer"), Some("int64"), _, None, None)) => query[Long](name)
      case Some(Schema(Some("number"), Some("float"), _, None, None)) => query[Float](name)
      case Some(Schema(Some("number"), Some("double"), _, None, None)) => query[Double](name)
      case Some(Schema(Some("boolean"), None, _, None, None)) => query[Boolean](name)
      case _ => query[String](name)
    }
  }.description(p.description.getOrElse(""))

  def defineOutput(ep: Endpoint[_, _, _, _], r: Map[String, Option[Response]], c: Option[Components]): Endpoint[_, _, _, _] =
    defineErrorOutput(
      defineSuccessOutput(
        ep,
        r.getOrElse(r.keySet.filter(_.startsWith("2")).minOption.getOrElse("None"), None),
        c),
      r.getOrElse("default", r.getOrElse(r.keySet.filter(_.startsWith("4")).minOption.getOrElse("None"), None)),
      c)

  def defineSuccessOutput(ep: Endpoint[_, _, _, _], r: Option[Response], c: Option[Components]): Endpoint[_, _, _, _] =
    if (r.isDefined) ep.out(captureOutput(r.get, c)) else ep

  def captureOutput(r: Response, c: Option[Components]): EndpointOutput[_] = {
    if (r.content.getOrElse(Map.empty).contains("application/json"))
      dereferenceRight(r.content.get("application/json").schema.get, c) match {
        case Some(Schema(Some("integer"), Some("int32"), _, None, None)) => jsonBody[Int]
        case Some(Schema(Some("integer"), Some("int64"), _, None, None)) => jsonBody[Long]
        case Some(Schema(Some("number"), Some("float"), _, None, None)) => jsonBody[Float]
        case Some(Schema(Some("number"), Some("double"), _, None, None)) => jsonBody[Double]
        case Some(Schema(Some("boolean"), None, _, None, None)) => jsonBody[Boolean]
        case _ => jsonBody[String]
      }
    else
      stringBody
  }.description(r.description)

  def defineErrorOutput(ep: Endpoint[_, _, _, _], r: Option[Response], c: Option[Components]): Endpoint[_, _, _, _] =
    if (r.isDefined) ep.errorOut(captureOutput(r.get, c)) else ep
}
