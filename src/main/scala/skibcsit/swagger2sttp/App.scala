package skibcsit.swagger2sttp

import sttp.tapir._

import scala.util.{Success, Failure}

object App {
  def main(args: Array[String]): Unit = {
    // example
    println(endpoint.in("folder").in(query[Int]("count")).out(stringBody).errorOut(plainBody[Int]).name("example").show)
    // parsing
    Parser.parse("petstore-expanded.yaml") match {
      case Success(value) => value.map(_.show).foreach(println)
      case Failure(exception) => println(exception)
    }

  }
}
