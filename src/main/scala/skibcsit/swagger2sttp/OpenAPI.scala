package skibcsit.swagger2sttp



case class Info(
                 description: Option[String] = None,
                 termsOfService: Option[String] = None,
                 contact: Option[Contact] = None,
                 license: Option[License] = None,
                 title: String,
                 version: String
               )

case class Contact(name: Option[String] = None, url: Option[String] = None, email: Option[String] = None)

case class License(name: String, url: Option[String] = None)

case class Server(url: String, description: Option[String] = None, variables: Option[Map[String, ServerVariable]] = None)

case class ServerVariable(enum: Option[List[String]] = None, default: String, description: Option[String] = None)

case class ExternalDocumentation(description: Option[String] = None, url: String)

case class Tag(name: String, description: Option[String] = None, externalDocs: Option[ExternalDocumentation] = None)

case class Components(
                       schemas: Option[Map[String, Either[Schema, Reference]]] = None,
                       responses: Option[Map[String, Either[Response, Reference]]] = None,
                       parameters: Option[Map[String, Either[Parameter, Reference]]] = None,
                       //examples: Option[Map[String, Either[Example, Reference]]] = None,
                       //requestBodies: Option[Map[String, Either[RequestBody, Reference]]] = None,
                       headers: Option[Map[String, Either[Header, Reference]]] = None,
                       //securitySchemes: Option[Map[String, Either[SecurityScheme, Reference]]] = None,
                       //links: Option[Map[String, Either[Link, Reference]]] = None,
                       //callbacks: Option[Map[String, Either[Map[String, PathItem], Reference]]] = None
                     )

case class PathItem(
                     get: Option[Operation] = None,
                     post: Option[Operation] = None,
                     put: Option[Operation] = None,
                     delete: Option[Operation] = None,
                     head: Option[Operation] = None,
                     patch: Option[Operation] = None,
                     trace: Option[Operation] = None,
                     //$ref: Option[String] = None,
                     summary: Option[String] = None,
                     description: Option[String] = None,
                     //options: Option[Operation] = None,
                     //servers: Option[List[Server]] = None,
                     parameters: Option[List[Either[Parameter, Reference]]] = None,
                   )

case class Operation(
                      description: Option[String] = None,
                      parameters: Option[List[Either[Parameter, Reference]]] = None,
                      requestBody: Option[Either[RequestBody, Reference]] = None,
                      operationId: Option[String] = None,
                      responses: Responses,
                      //externalDocs: Option[ExternalDocumentation] = None,
                      //callbacks: Option[Map[String, Either[Callback, Reference]]] = None,
                      deprecated: Option[Boolean] = None,
                      //security: Option[SecurityRequirement] = None,
                      //servers: Option[List[Server]] = None,
                      summary: Option[String] = None,
                      tags: Option[List[String]] = None,
                    )

case class Parameter(
                      name: String,
                      in: String,
                      description: Option[String] = None,
                      required: Boolean,
                      schema: Option[Either[Schema, Reference]] = None,
                      //deprecated: Option[Boolean],
                      //allowEmptyValue: Option[Boolean] = None,
                      //style: Option[String] = None,
                      //explode: Option[Boolean] = None,
                      //allowReserved: Option[Boolean] = None,
                      //example: Option[String] = None,
                      //examples: Option[Map[String, Either[Example, Reference]]] = None,
                      //content: Option[Map[String, Schema]] = None
                    )


case class Response(
                     description: String,
                     content: Option[Map[String, MediaType]] = None,
                     //headers: Option[Map[String, Either[Header, Reference]]] = None,
                     //links: Option[Map[String, Either[Link, Reference]]] = None,
                   )


case class Header(
                   description: Option[String] = None,
                   schema: Option[Either[Schema, Reference]] = None,
                 )


case class Reference($ref: String)

case class Schema(
                   `type`: Option[String] = None,
                   format: Option[String] = None,
                   required: Option[List[String]] = None,
                   items: Option[Either[Schema, Reference]] = None,
                   properties: Option[Map[String, Schema]] = None
                 )


case class RequestBody(
                        description: Option[String] = None,
                        content: Map[String, MediaType],
                        required: Option[Boolean] = None
                      )

case class MediaType(
                      schema: Option[Either[Schema, Reference]] = None,
                      //example: Option[String] = None,
                      //examples: Option[Map[String, Either[Example, Reference]]] = None,
                      //encoding: Option[Map[String, Encoding]] = None
                    )

/*

case class Encoding(
                     contentType: Option[String] = None,
                     headers: Option[Map[String, Either[Header, Reference]]] = None,
                     style: Option[String] = None,
                     explode: Option[Boolean] = None,
                     allowReserved: Option[Boolean] = None
                   )

case class Example(
                    summary: Option[String] = None,
                    description: Option[String] = None,
                    value: Option[String] = None,
                    externalValue: Option[String] = None
                  )

case class Link(
                 operationRef: Option[String] = None,
                 operationId: Option[String] = None,
                 parameters: Option[Map[String, String]] = None,
                 requestBody: Option[String] = None,
                 description: Option[String] = None,
                 server: Option[Server] = None
               )



case class SecurityScheme(
                           `type`: String,
                           description: Option[String] = None,
                           name: String,
                           in: String,
                           scheme: String,
                           bearerFormat: Option[String] = None,
                           flows: OAuthFlows,
                           openIdConnectUrl: String
                         )

case class Discriminator(
                          propertyName: String,
                          mapping: Option[Map[String, String]] = None
                        )

case class XML(
                name: Option[String] = None,
                namespace: Option[String] = None,
                prefix: Option[String] = None,
                attribute: Option[String] = None,
                wrapped: Option[Boolean] = None
              )

case class OAuthFlows(
                       `implicit`: Option[OAuthFlow] = None,
                       password: Option[OAuthFlow] = None,
                       clientCredentials: Option[OAuthFlow] = None,
                       authorizationCode: Option[OAuthFlow] = None
                     )

case class OAuthFlow(
                      authorizationUrl: String,
                      tokenUrl: String,
                      refreshUrl: Option[String] = None,
                      scopes: Map[String, String]
                    )
 */

sealed trait OpenAPI

case class OpenAPI3(servers: Option[List[Server]] = None,
                    components: Option[Components] = None,
                    security: Option[SecurityRequirement] = None,
                    tags: Option[List[Tag]] = None,
                    externalDocs: Option[ExternalDocumentation] = None,
                    paths: Paths,
                    openapi: String,
                    info: Info,
                   ) extends OpenAPI